-module(filegaps).
-export([run/0, gapsWorker/3, addCount/1]).
-import(array, [map/2]).

% Reads input from input file and gets gaps from files with gap constraint G
run() ->
  Input = readInput("input.txt"),
  K = list_to_integer(lists:nth(2, Input)),
  G = list_to_integer(lists:nth(1, Input)),
  Files = tl(tl(Input)),

  Main_PID = self(),
  ThreadCount = length(Files),
  % create worker threads which process the files
  lists:foreach(fun(FileName) -> 
                  spawn(filegaps, gapsWorker, [FileName, G, Main_PID])
                end, Files),
  % wait here for all the results from the threads
  Results = waitThreads(ThreadCount, []),

  % reduce the results of all threads to one result
  Gaps = reduceGaps(lists:map( fun(Res) -> hd(Res) end, Results), K),
  LineCount = lists:foldl( fun(Res, Sum) -> lists:last(Res) + Sum end, 0,Results),

  % write to output file
  writeOutput(Gaps, LineCount),

  io:fwrite("done").
  
waitThreads(0, Res) -> Res;
waitThreads(N, Res) ->
  receive
    Result ->
      NewRes = lists:append(Result, Res),
      waitThreads(N-1, NewRes)
end.

gapsWorker(FileName, G, Main_PID) ->
	link(Main_PID),
  Lines = readInput(FileName),
  LineCount = length(Lines),
  Gaps = lists:map(fun(Line) ->
                  removeDuplicates(allGaps(G, Line))
                end, Lines),
  Result = [[Gaps, LineCount]],
  % send results back to main thread
	Main_PID ! Result.

removeDuplicates(Ls) ->
    Set = sets:from_list(Ls),
    sets:to_list(Set).

% gets all character pairs from line with gap constraint G
allGaps(G, Str) -> allGaps2(G, Str, (length(Str) - G)).
allGaps2(_, _, N) when 0 >= N -> [];
allGaps2(G, [X|Xs], N) ->
  lists:append(allGaps2(G, Xs, N-1), [{X, lists:nth(G-1, Xs)}]).

reduceGaps(Gaps, K) ->
  Groups = group( lists:sort(lists:flatten(Gaps))),
  Counts = lists:map(fun(X) -> addCount(X) end, Groups),
  Sorted = lists:sort(fun({_, A}, {_, B}) -> A >= B end, Counts),
  lists:sublist(Sorted, K).

% groups tuples in lists by equality of values
group(List) ->
  lists:map( fun({_,Y}) -> Y end,
    dict:to_list(lists:foldr(fun({X,Y}, D) -> dict:append(X, Y, D) end , dict:new(), [ {X, X} || X <- List ]))).

addCount(Ls) ->
  {Ls, length(Ls)}.

writeOutput(Res, LineCount) ->
  lists:foreach(fun(Line) ->
                  Pair = hd(element(1, Line)),
                  file:write_file("output.txt", io_lib:fwrite("~c ~c ~p ~p\n", [element(1, Pair), element(2, Pair), element(2, Line), LineCount ]), [append])
                end, Res),
  0.
  
% reads inputfile and splits lines into an array
readInput(InputFile) ->
  {ok, Data} = file:read_file(InputFile),
  string:tokens(erlang:binary_to_list(Data), "\n").
