import System.Environment
import System.Directory
import System.IO
import Data.List.Split
import Data.List
import Control.Concurrent
import Control.Monad

inputFile = "input.txt"
outputFile = "output.txt"

main = do
  -- open input file and read the arguments
  contents <- readFile inputFile
  let input = (splitOn "\n" contents)
      g = read (input !! 0) ::Int -- the gap constraint
      k = read (input !! 1) ::Int -- the number of most frequent pairs
      files = drop 2 input
  --mvars fro gap results and linecount
  m <- newMVar ([])
  mLineCount <- newMVar (0 :: Int)
  --create a thread for each filee
  threads <- forM files (\x -> makeThread (getFileGaps g x m mLineCount))
  -- Execute threads and wait for them all to finish using the mvars
  _ <- mapM_ takeMVar threads
  -- get results from mvar
  results <- takeMVar m
  --get line count
  allLines <- takeMVar mLineCount

  --reduce the result and print in the file
  writeOutput (reduceGaps k results) allLines
  print "done"

-- creates a thread which completion can be monitored with the mvar
makeThread proc = do
    handle <- newEmptyMVar
    _ <- forkFinally proc (\_ -> putMVar handle ())
    return handle

-- reads file and gets all the gaps from each of its lines
getFileGaps g fileName m mLineCount = do
  contents <- readFile fileName
  let lines = (splitOn "\n" contents)
      gaps = getGaps g lines
  list <- takeMVar m
  putMVar m (list ++ gaps)
  allLines <- takeMVar mLineCount
  putMVar mLineCount (allLines + (length lines))
  print ("File " ++ fileName ++ " processed")

getGaps :: Int -> [String] -> [[(Char, Char)]]
getGaps g inputStrs =
  -- Count the gaps on each row
  map (\x -> removeDubs (allGaps g x )) inputStrs

--gets all the gaps from one line
allGaps :: Int -> String -> [(Char, Char)]
allGaps g str = allGaps' g str ((length str) - g)
allGaps' :: Int -> String -> Int -> [(Char, Char)]
allGaps' _ _ 0 = []
allGaps' _ [] _ = []
allGaps' g (x:xs) i = 
  (x, xs !! (g-1)) : allGaps' g xs (i-1)

removeDubs arr = map head (group (sort arr))

writeOutput output nlines = do
  let lines = map (makeOutputLine nlines) output
      result = concat lines
  appendFile outputFile result

--formats the output to string
makeOutputLine :: Int -> ((Char, Char), Int) -> String
makeOutputLine n x =
  (fst (fst x)): ' ': (snd (fst x)) : ' ': [] ++ (show (snd x)) ++ " " ++ (show n) ++ "\n"

-- calculates the most frequent gaps and takes only the k most frequent
reduceGaps k gaps =
  let groups = groupBy (\a b -> fst a == fst b && snd a == snd b) (sort (concat gaps))
      counts = map (\x -> getCount x) groups
      sorted = sortBy sortTuples counts
  in take k sorted

getCount arr =
  ((head arr), length arr)

sortTuples (_, a) (_, b)
  | a < b = GT
  | a > b = LT
  | a == b = EQ